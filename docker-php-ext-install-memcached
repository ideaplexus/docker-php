#!/usr/bin/env sh

CONFIGURE_ARGS=""

MEMCACHED_VERSION=`wget -q -O - "https://api.github.com/repos/php-memcached-dev/php-memcached/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/'`
if [[ $(php -r "echo PHP_MAJOR_VERSION;") == 5 ]]; then
  MEMCACHED_VERSION="2.2.0"
fi

while test $# -gt 0
do
    case "$1" in
        --enable-memcached-igbinary)
            CONFIGURE_ARGS="${CONFIGURE_ARGS} --enable-memcached-igbinary"
            pecl install igbinary
            ;;
        --enable-memcached-json)
            CONFIGURE_ARGS="${CONFIGURE_ARGS} --enable-memcached-json"
            ;;
        --enable-memcached-msgpack)
            CONFIGURE_ARGS="${CONFIGURE_ARGS} --enable-memcached-msgpack"
            pecl install msgpack
            ;;
        --*) echo "bad option: $1"
            exit 1
            ;;
    esac
    shift
done

set -eux \
  && apk add --no-cache \
    libmemcached-dev \
    zlib-dev \
  && wget -q -O /tmp/memcached.tar.gz https://api.github.com/repos/php-memcached-dev/php-memcached/tarball/${MEMCACHED_VERSION} \
  && mkdir -p /tmp/memcached \
  && tar \
      --extract \
      --file /tmp/memcached.tar.gz \
      --directory /tmp/memcached \
      --strip-components 1 \
  && cd /tmp/memcached \
  && phpize \
  && ./configure ${CONFIGURE_ARGS} \
  && make \
  && make install \
  && rm -rf /tmp/memcached* \
  && docker-php-ext-enable --ini-name zz-docker-php-ext-memcached-ini memcached # ensure that memcached is loaded after igbinary/msgpack
